<?php

function varAndDie($var)
{

    var_dump($var) and die();
}


/**
 * Function to get the current page url with query string
 *
 * @param  string $string
 * @return boolean
 */


function isActive($string)
{
    if (strpos($_SERVER['REQUEST_URI'], $string) !== false) {
        return true;
    } else {
        return false;
    }
}
