<?php

use Tamtamchik\SimpleFlash\Flash;

class Users extends Controller
{

    private $userModel;

    public function __construct()
    {


        $this->userModel = $this->model('User');
    }

    // Método para registrarse en la BBDD
    public function register()
    {

        // Si el request es de tipo POST
        if ($_SERVER['REQUEST_METHOD'] === 'POST') {


            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);
            // Comprobamos que los campos no estén vacíos
            $data['name'] = htmlentities($_POST['name']) ?? '';
            $data['email'] = htmlentities($_POST['email']) ?? '';
            $data['password'] = htmlentities($_POST['password']) ?? '';
            $data['confirm_password'] = htmlentities($_POST['confirm_password']) ?? '';

            // Trim todos los indices
            $data = array_map('trim', $data);

            // Validamos los campos

            empty($data['name']) ? $data['name_err'] = 'El nombre es obligatorio' : '';
            empty($data['email']) ? $data['email_err'] = 'El email es obligatorio' : '';
            empty($data['password']) ? $data['password_err'] = 'La contraseña es obligatoria' : '';
            empty($data['confirm_password']) ? $data['confirm_password_err'] = 'La confirmación de la contraseña es obligatoria' : '';

            // Validamos email

            if (!empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'El email no es válido';
            }

            if (!empty($data['email']) && $this->userModel->findUserByEmail($data['email'])) {
                $data['email_err'] = 'El email ya existe en la base de datos';
            }

            // Validamos que las contraseñas coincidan

            if (!empty($data['password']) && !empty($data['confirm_password'])) {
                if ($data['password'] !== $data['confirm_password']) {
                    $data['confirm_password_err'] = 'Las contraseñas no coinciden';
                }
            }

            // Validamos contraseña al menos 6 chars. 

            if (!empty($data['password']) && strlen($data['password']) < 6) {
                $data['password_err'] = 'La contraseña debe tener al menos 6 caracteres';
            }

            // Si no hay errores, ejecutamos la inserción en la base de datos

            if (empty($data['name_err']) && empty($data['email_err']) && empty($data['password_err']) && empty($data['confirm_password_err'])) {



                try {
                    $this->userModel->register($data);
                    $flash = new Flash();
                    $flash->message('¡Registro correcto! Puedes iniciar sesión.');
                    urlRedirect('/users/login');
                } catch (\PDOException $e) {
                    throw $e;
                }
            } else {
                // Si hay errores, mostramos los errores
                $this->view('users/register', $data);
            }
        } else {
            // Si no, pasamos a la vista un array con todas las posibles variables relacionadas con el registro de usuarios. 
            // Si el método es GET, las variables estarán vacías. En los errores, se mantendrán en el array para la persistencia de datos. 

            $data = [
                'name' => '',
                'email' => '',
                'password' => '',
                'confirm_password' => '',
                'name_err' => '',
                'email_err' => '',
                'password_err' => '',
                'confirm_password_err' => ''

            ];

            // Cargamos la vista buscando la carpeta /users/register.php y le pasamos el array de datos. 
            $this->view('users/register', $data);
        }
    }
    // Método para iniciar sesión
    public function login()
    {

        if ($_SERVER['REQUEST_METHOD'] === 'POST') {

            // Sanitiza
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Capturamos los datos del formulario
            $data['email'] = htmlentities($_POST['email']) ?? '';
            $data['password'] = htmlentities($_POST['password']) ?? '';

            // Trim todos los indices
            $data = array_map('trim', $data);

            // Comprobamos que los datos estén introducidos

            empty($data['email']) ? $data['email_err'] = 'El email es obligatorio' : '';
            empty($data['password']) ? $data['password_err'] = 'La contraseña es obligatoria' : '';

            // Validación de email
            if (!empty($data['email']) && !filter_var($data['email'], FILTER_VALIDATE_EMAIL)) {
                $data['email_err'] = 'El email no es válido';
            }

            if (!empty($data['password']) && strlen($data['password']) < 6) {
                $data['password_err'] = 'La contraseña debe tener al menos 6 caracteres';
            }

            // Si al buscar el email devuelve true
            if ($this->userModel->findUserByEmail($data['email'])) {
                // TO DO
            } else {
                $data['email_err'] = 'No se ha encontrado un usuario con ese correo';
            }


            if (empty($data['email_err']) && empty($data['password_err'])) {

                $user = $this->userModel->login($data['email'], $data['password']);
               

                if ($user) {

                    $this->createUserSesion($user);
                } else {
                    $data['password_err'] = 'Contraseña incorrecta';
                    $this->view('users/login', $data);
                }
            }
        } else {

            $data = [
                'email' => '',
                'password' => '',
                'email_err' => '',
                'password_err' => ''
            ];

            // Devuelve la vista con los datos del array. 
            $this->view('users/login', $data);
        }
    }
    // Función para crear una sesión de usuario
    public function createUserSesion($user)
    {

        $_SESSION['id'] = $user->id;
        $_SESSION['name'] = $user->name;
        $_SESSION['email'] = $user->email;

        urlRedirect('/posts/index');
    }

    //Función para cerrar la sesión de usuario
    public function logout()
    {
        unset($_SESSION['id']);
        unset($_SESSION['name']);
        unset($_SESSION['email']);
        session_destroy();
        urlRedirect('/users/login');
    }
}
