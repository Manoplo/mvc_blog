<?php

use Tamtamchik\SimpleFlash\Flash;

class Posts extends Controller
{

    private $postModel;
    private $userModel;
    
    

    public function __construct()
    {
        if (!isLoggedIn()) {
            urlRedirect('/users/login');
        }

        
        $this->postModel = $this->model('Post');
        $this->userModel = $this->model('User');
    }


    public function index()
    {

        $posts = $this->postModel->getPosts();

        $data = [
            'titulo' => 'Index de posts',
            'posts' => $posts
        ];

        return $this->view('posts/index', $data);
    }

    public function add()
    {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitiza el array POST
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Creamos un array con los campos del formulario, el user 
            $data = [
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['id'],
                'image' => !empty($_FILES) ? $_FILES['image']['name'] : '', // LA VARIABLE GLOBAL $_FILES ES UN ARRAY ASOCIATIVO CON LOS DATOS DE LOS ARCHIVOS ENVIADOS POR UN FORMULARIO A TRAVÉS DE UN INPUT TYPE FILE. 
                'image_err'=>'',
                'title_err' => '',
                'body_err' => ''
            ];

            // Si los campos están vacíos, insertamos los errores. 

            empty($data['title']) ? $data['title_err'] = 'El título es obligatorio' : '';
            empty($data['body']) ? $data['body_err'] = 'El cuerpo del post es obligatorio' : '';


            // Si el archivo no está vacío...
            if(!empty($data['image'])){
               
                $arrayTypes = ['image/jpeg', 'image/png', 'image/gif'];
                $fileArray = $_FILES['image'];
          

                try {

                    $file = new File($fileArray, $arrayTypes );
                    $file->validate();
                    $file->saveUploadedFile('img/');
                    
                } catch (FileException $error) {
                    
                    $data['image_err'] = $error->getMessage();
                    

                }
            }
                
            // Si los campos de error están vacíos, insertamos. 
            if (empty($data['title_err']) && empty($data['body_err']) && empty($data['image_err'])) {
                
            
                try {
                    $this->postModel->addPost($data);
                    $flash = new Flash();
                    $flash->message('¡Post creado correctamente!');
                    urlRedirect('/posts/index');
                } catch (\PDOException $err) {
                    throw $err;
                }
            }else{
                // Si los campos de error no están vacíos, mostramos los errores. 
                $this->view('posts/add', $data);
            }
        } else {

            $data = [
                'title' => '',
                'body' => '',
                'user_id' => '',
                'title_err' => '',
                'body_err' => '',
            ];

            return $this->view('posts/add', $data);
        }
    }

    public function show($id)
    {
        $post = $this->postModel->getPostById($id);
        $user = $this->userModel->getUserById($post->user_id);

        $data = [
            'user' => $user,
            'post' => $post
        ];

        return $this->view('posts/show', $data);
    }
    
    public function edit($id){

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {
            // Sanitiza el array POST
            $_POST = filter_input_array(INPUT_POST, FILTER_SANITIZE_STRING);

            // Creamos un array con los campos del formulario, el user 
            $data = [
                'id' => $id,
                'title' => trim($_POST['title']),
                'body' => trim($_POST['body']),
                'user_id' => $_SESSION['id'],
                'title_err' => '',
                'body_err' => ''
            ];

            // Si los campos están vacíos, insertamos los errores. 

            empty($data['title']) ? $data['title_err'] = 'El título es obligatorio' : '';
            empty($data['body']) ? $data['body_err'] = 'El cuerpo del post es obligatorio' : '';

            // Si los campos de error están vacíos, insertamos. 
            if (empty($data['title_err']) && empty($data['body_err'])) {

                try {
                    $this->postModel->updatePost($data);
                    $flash = new Flash();
                    $flash->message('¡Post actualizado correctamente!');
                    urlRedirect('/posts/index');
                } catch (\PDOException $err) {
                    throw $err;
                }
            }else{
                // Si los campos de error no están vacíos, mostramos los errores. 
                $this->view('posts/edit', $data);
            }
        } else {

            $post = $this->postModel->getPostById($id);

            if ($post->user_id !== $_SESSION['id']) {
                $flash = new Flash();
                $flash->error('No tienes permisos para editar este post');
                urlRedirect('/posts/index');
            }

            $data = [
                'id' => $id,
                'title' => $post->title,
                'body' => $post->body,
                'user_id' => $post->user_id,
            ];

            return $this->view('posts/edit', $data);
        }

    }

    public function delete($id){
        

        if( $_SERVER['REQUEST_METHOD'] === 'POST' ){

            $post = $this->postModel->getPostById($id);

            if ($post->user_id !== $_SESSION['id']) {
                $flash = new Flash();
                $flash->error('No tienes permisos para eliminar este post');
                urlRedirect('/posts/index');
            }else{

                try {
                    $this->postModel->deletePost($post->id);
                    $flash = new Flash();
                    $flash->message('Post eliminado correctamente');
                    urlRedirect('/posts/index');
                } catch (\PDOException $err) {
                    throw $err;
                }
                
            }
        }else{
            
            $flash = new Flash();
            $flash->error('No tienes permisos para eliminar este post');
            urlRedirect('/posts/index');
        }
    }
}
