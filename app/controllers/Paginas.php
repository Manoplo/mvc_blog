<?php

class Paginas extends Controller

{
    public function __construct()
    {

        // Desde aquí cargaremos los modelos // ------------->


    }

    public function index()
    {
        if (isLoggedIn()) {
            urlRedirect('/posts/index');
        }

        $data = [
            'titulo' => 'Bienvenido al Blog-MVC de Manuel Martín'
        ];

        return $this->view('paginas/index', $data);
    }

    public function about()
    {

        $data = [
            'cabecera' => 'Sobre nosotros.'
        ];

        return $this->view('paginas/about', $data);
    }
}
