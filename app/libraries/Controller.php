<?php


class Controller
{

    public function model($model)
    {
        // Carga el modelo y lo devuelve
        require_once '../app/models/' . $model . '.php';
        return new $model;
    }

    public function view($vista, $data = [])
    {
        // Carga la vista y le pasa los datos. Si no hay datos, los pasa vacíos. Estos datos son variables que se pasan a la vista. 
        if (file_exists('../app/views/' . $vista . '.view.php')) {

            require '../app/views/' . $vista . '.view.php';
        } else {
            die('La vista no existe');
        }
    }
}
