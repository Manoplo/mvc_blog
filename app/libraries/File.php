<?php

require_once APPROOT . '/exceptions/FileException.php';

class File{

    private $fileArray;
    private $arrayTypes; // VARIABLE QUE CONTIENE LOS MIME TYPES O TIPOS ASOCIADOS DE ARCHIVOS PERMITIDOS. 


        public function __construct($fileArray, $arrayTypes){

            $this->fileArray = $fileArray;
            $this->arrayTypes = $arrayTypes;
           
            
        }

        public function validate(){

            if($this->fileArray['error'] !== UPLOAD_ERR_OK){
                switch($this->fileArray['error']){
                    // SI EL ARCHIVO ES MAS GRANDE DE LO QUE PERMITE LA DIRECTIVA DE configuración MAX_FILE_SIZE
                    case UPLOAD_ERR_INI_SIZE:
                    case UPLOAD_ERR_FORM_SIZE:
                        throw new FileException('El archivo es demasiado grande. Tamaño máximo:'.ini_get('upload_max_filesize')); // LA FUNCIÓN INI_GET() DEVUELVE EL VALOR DE UNA DIRECTIVA DE configuración.

                        break;
                    // SI EL ARCHIVO SE CARGÓ PARCIALMENTE
                    case UPLOAD_ERR_PARTIAL:
                        throw new FileException('No se pudo subir el fichero completo');
                        
                        break;
                    default:
                        throw new FileException('Error al subir el fichero');
                        
                        break;
                }
             }
         }

        public function saveUploadedFile($path){
                // in_array() comprueba si un elemento está en un array
            if( in_array($this->fileArray['type'], $this->arrayTypes) === false && $this->fileArray['name'] !== '' ){
                // SI NO ES UN ARCHIVO DE TIPO PERMITIDO
                throw new FileException('Tipo de archivo no soportado');
            
            // SI NO SE HA SUBIDO POR EL FORMULARIO // La función is_uploaded_file() comprueba si un archivo se ha subido mediante el método HTTP POST.
            }elseif(is_uploaded_file($this->fileArray['tmp_name']) === false){

                throw new FileException('El archivo no se ha subido mediante formulario');
                
            // SI NO HAY NINGÚN ERROR INESPERADO Y SE HA MOVIDO A LA CARPETA ASIGNADA // El método move_uploaded_file() mueve un archivo subido mediante el método HTTP POST a una ruta de destino.
            }elseif(move_uploaded_file($this->fileArray['tmp_name'], $path . $this->fileArray['name']) === false){

                throw new FileException('Hubo un error y el archivo no se pudo guardar');
            

            }
        }
    
}