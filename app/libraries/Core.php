<?php

class Core
{

    protected $controladorActual = 'Paginas'; // Default controller

    protected $metodoActual = 'index'; //   Default method

    protected $params = []; // No default params

    // CONSTRUCTOR

    public function __construct()
    {
        ////////////////////////////////////  MAPEAR CONTROLLER
        //Al instanciar un objeto de clase Core, se llama automáticamente al método getUrl()

        $url = $this->getUrl();

        // Si en la carpeta controllers existe un archivo con el primer nombre en mayúsculas que le entra por parámetros de URL...
        if (file_exists('../app/controllers/' . ucwords($url[0] . '.php'))) {

            // El controlador actual es el que ha entrado por parámetros
            $this->controladorActual = ucwords($url[0]);

            // Liberamos la variable 0 (Controlador) del array $url
            unset($url[0]);
        }

        // Llamo al archivo del controlador que ha entrado por parámetros para poder instanciarlo después. 
        require_once '../app/controllers/' . $this->controladorActual . '.php';

        // El controlador actual pasa de ser un string a un objeto instanciado. 
        $this->controladorActual = new $this->controladorActual;

        /////////////////////////////////////   MAPEAR MÉTODO

        // Si se le ha introducido un segundo parámetro a la URL...
        if (isset($url[1])) {

            // Checkea si el método de una clase existe. Espera la clase a observar como primer parámetro y el método como segundo. 
            if (method_exists($this->controladorActual, $url[1])) {

                // La variable metodoActual pasa a ser el método pasado por parámetros de la URL
                $this->metodoActual = $url[1];

                // Libera la segunda posición del array $url
                unset($url[1]);
            }
        }


        ////////////////////////////////////  MAPEAR PARAMS

        // Si queda algún valor en el array (hemos unseteado los anteriores), los parámetros son el valor(sin la key) que quedan en el array $url, y si no, es un array vacío
        $this->params = $url ? array_values($url) : [];

        // LLama al controlador, llama a su función y le pasa los parámetros de manera dinámica. 
        call_user_func_array([$this->controladorActual, $this->metodoActual], $this->params);
    }



    public function getUrl()
    {

        // Si existe una URL pasada por parámetros...
        if (isset($_GET["url"])) {

            // Hazle un trim por el final y elimina '/' si el usuario escribe /hola/pepe/, por ej. 

            $url = rtrim($_GET["url"], '/');

            // Sanitiza la URL y elimina caracteres ilegales
            $url = filter_var($url, FILTER_SANITIZE_URL);

            // Divide la url en un array, usando el '/' como separador y eliminándolo del propio array. 

            $url = explode('/', $url);
        } else { // Si no se pasa una url por parámetros...

            // La posición 0 del array $url es el string "Paginas"
            $url[0] = "Paginas";
        }

        // DESCOMENTAR PARA VER EL ARRAY QUE LE LLEGA POR URL
        /* print_r($url); */
        return $url;
    }
}
