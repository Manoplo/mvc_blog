<?php

// Start a Session
if( !session_id() ) @session_start();

// Initialize Composer Autoload
require_once 'vendor/autoload.php';


// Cargar clase Dotenv

$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

spl_autoload_register(function ($className) {
    require_once 'libraries/' . $className . '.php';
});

//carga FileException

/* require_once './exceptions/FileException.php'; */

// carga Paginas.php 

require_once '../app/controllers/Paginas.php';

// carga config.php

require_once '../app/config/config.php';

// carga utils.php

require_once '../app/helpers/utils.php';

// carga urlRedirect.php

require_once '../app/helpers/urlRedirect.php';

// carga isLoggedIn.php

require_once '../app/helpers/isLoggedIn.php';
