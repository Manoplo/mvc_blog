<?php require_once APPROOT . '/views/partials/header.php'; ?>
<div class="container">

    <div class="row mb-3">
        <div class="flashes">
            <?= (string) flash() ?>
        </div>
        <div class="col-md-6">
            <h1>Publicaciones</h1>
        </div>
        <div class="col-md-6">
            <a class="btn btn-primary pull-right" href="<?= URLROOT . '/posts/add' ?>" role="button">
                <i class="fas fa-pencil-alt"></i> Crear publicación
            </a>
        </div>
        <?php foreach ($data['posts'] as $post) : ?>
            <div class="card">
                <div class="card-header">
                    Publicado por <?= $post->name ?> el <?= $post->postCreatedAt ?>

                </div>
                <div class="card-body">
                    <?php if(($post->image)) : ?>
                        <img src="<?= URLROOT . '/public/img/' . $post->image ?>" style="float:left; margin-right:15px" width="200px"alt="">
                    <?php endif; ?>
                    <h5 class="card-title"><?= $post->title ?></h5>
                    <p class="card-text"><?= $post->body ?></p>
                    <a href="<?= URLROOT . "/posts/show/$post->postId" ?>" class="btn btn-primary">Leer más</a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

<?php require_once APPROOT . '/views/partials/footer.php'; ?>