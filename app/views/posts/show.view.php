<?php require_once APPROOT . '/views/partials/header.php'; ?>

<a class="btn btn-warning pull-right" href="<?=URLROOT . '/posts/index'?>" role="button">
    <i class="fas fa-arrow-left"></i> Regresar
</a>
<br>
<div class="row mb-3">
    <div class="col-md-12">
        <h1><?= $data['post']->title ?></h1>

        <div class="bg-secondary text-white p-2 mb-3">
            Creado por: <?= $data['user']->name ?>  el <?= $data['post']->created_at ?>
        </div>
        <?php if(($data['post']->image)) : ?>
                        <img src="<?= URLROOT . '/public/img/' . $data['post']->image ?>" width="600px" alt="">
        <?php endif;?>
        <p>
        <?= $data['post']->body ?>
        </p>
        
            <hr>
            <?php if($data['post']->user_id === $_SESSION['id']) : ?>
            <div class="row">
                <div class="col">
                    <a href="<?= URLROOT .'/posts/edit/'. $data['post']->id ?>" class="btn btn-success btn-block">
                        <i class="fas fa-edit"></i> editar
                    </a>
                </div>
                <div class="col">
                    <form action="<?= URLROOT.'/posts/delete/'.$data['post']->id ?>" method="post">                        
                        <button type="submit" class="btn btn-danger btn-block">
                            <i class="fas fa-trash"></i> Borrar post
                        </button>
                    </form>
                </div>
            </div>
            <?php endif; ?>
    </div>
</div>



<?php require_once APPROOT . '/views/partials/footer.php'; ?>