<?php require_once APPROOT . '/views/partials/header.php'; ?>

<div class="p-5 mb-4 bg-light rounded-3">
    <div class="container-fluid py-5">
        <h1 class="display-5"><?= isset($data['cabecera']) ?$data['cabecera'] : 'Bienvenido'?></h1>
        <p class="col-md-8 fs-4">Lorem ipsum dolor sit amet consectetur adipisicing elit. Rerum quos, nostrum voluptatem voluptate esse error mollitia fuga iusto ducimus nemo doloribus tempore nobis vel recusandae aspernatur. Animi at dolore magnam?</p>
        <p>Version: <span style="color: white; background-color: green; border-radius: 10px; padding: 5px;"><?= APPVERSION ?></span></p>
    </div>
</div>
<?php require_once APPROOT . '/views/partials/footer.php'; ?>