<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <div class="container-fluid">
    <a class="navbar-brand" href="#">MVCBlog</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav">
        <li class="nav-item">
          <a class="nav-link <?= isActive('index') ? 'active' : '' ?>" aria-current="page" href="<?= URLROOT . '/paginas/index' ?>">Home</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= isActive('about') ? 'active' : '' ?> " href="<?= URLROOT . '/paginas/about' ?>">About</a>
        </li>
      </ul>
    </div>
    <ul class="navbar-nav">
      <?php if (isset($_SESSION['id'])) : ?>

        <li class="nav-item">
          <h5 class="mt-2 ml-3" style="color: white">Bienvenid@, <?= $_SESSION['name'] ?></h5>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="<?= URLROOT . '/users/logout' ?>">Logout</a>
        </li>
      <?php else : ?>
        <li class="nav-item">
          <a class="nav-link <?= isActive('register') ? 'active' : '' ?> " aria-current="page" href="<?= URLROOT . '/users/register' ?>">Register</a>
        </li>
        <li class="nav-item">
          <a class="nav-link <?= isActive('login') ? 'active' : '' ?>" href="<?= URLROOT . '/users/login' ?>">Login</a>
        </li>
    </ul>
  <?php endif; ?>

  </div>
</nav>