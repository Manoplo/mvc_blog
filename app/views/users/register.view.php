<?php require APPROOT . '/views/partials/header.php'; ?>
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <h2> Crear una cuenta</h2>
            <p>Por favor llena los campos para poder registrarse</p>
            <form method="POST" action="<?= URLROOT . '/users/register'; ?>">
                <div class="form-group">
                    <!--NAME FORM-->
                    <label for="name">Nombre: <sup>*</sup></label>
                    <input type="text" name="name" class="form-control <?php if (isset($data['name_err']) && $data['name_err'] === '') {
                                                                            echo '';
                                                                        } elseif (isset($data['name_err']) && $data['name_err'] !== '') {
                                                                            echo 'is-invalid';
                                                                        } else {
                                                                            echo 'is-valid';
                                                                        }   ?>" value="<?= isset($data['name']) ? $data['name'] : ''; ?>">

                    <?php if (isset($data['name_err'])) : ?>
                        <div id=" validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['name_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            ¡Nombre correcto!
                        </div>
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <!--EMAIL FORM-->
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="email" name="email" class="form-control <?php if (isset($data['email_err']) && $data['email_err'] === '') {
                                                                                echo '';
                                                                            } elseif (isset($data['email_err']) && $data['email_err'] !== '') {
                                                                                echo 'is-invalid';
                                                                            } else {
                                                                                echo 'is-valid';
                                                                            } ?>" value="<?= isset($data['email']) ? $data['email'] : '' ?>">

                    <?php if (isset($data['email_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['email_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            ¡Email válido!
                        </div>
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <!--PASSWORD FORM-->
                    <label for="password">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?php if (isset($data['password_err']) && $data['password_err'] === '') {
                                                                                    echo '';
                                                                                } elseif (isset($data['password_err']) && $data['password_err'] !== '') {
                                                                                    echo 'is-invalid';
                                                                                } else {
                                                                                    echo 'is-valid';
                                                                                } ?>" value="<?= isset($data['password']) ? $data['password'] : '' ?>">

                    <?php if (isset($data['password_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['password_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            !Contraseña válida!
                        </div>
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <!--CONFIRM PASSWORD FORM-->
                    <label for="confirm_password">Confirmar contraseña: <sup>*</sup></label>
                    <input type="password" name="confirm_password" class="form-control <?php if (isset($data['confirm_password_err']) && $data['confirm_password_err'] === '') {
                                                                                            echo '';
                                                                                        } elseif (isset($data['confirm_password_err']) && $data['confirm_password_err'] !== '') {
                                                                                            echo 'is-invalid';
                                                                                        } else {
                                                                                            echo 'is-valid';
                                                                                        }  ?> " value="<?= isset($data['confirm_password']) ? $data['confirm_password'] : '' ?>">
                    <?php if (isset($data['confirm_password_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['confirm_password_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            !Las contraseñas coinciden!
                        </div>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="<?= URLROOT . '/users/login'; ?>">¿Ya tienes cuenta? Inicia sesión</a>
                    </div>
                    <div class="col">
                        <input type="submit" value="Registrar" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require APPROOT . '/views/partials/footer.php'; ?>