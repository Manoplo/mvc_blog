<?php require APPROOT . '/views/partials/header.php'; ?>
<div class="row">
    <div class="col-md-6 mx-auto">
        <div class="card card-body bg-light mt-5">
            <div class="flashes">
                <?= (string) flash() ?>
            </div>

            <h2>Iniciar sesión</h2>
            <p>Por favor, introduzca su correo y contraseña</p>
            <form method="POST" action="<?= URLROOT . '/users/login' ?>">
                <div class="form-group">
                    <label for="email">Email: <sup>*</sup></label>
                    <input type="email" name="email" class="form-control <?php if (isset($data['email_err']) && $data['email_err'] === '') {
                                                                                echo '';
                                                                            } elseif (isset($data['email_err']) && $data['email_err'] !== '') {
                                                                                echo 'is-invalid';
                                                                            } else {
                                                                                echo 'is-valid';
                                                                            } ?> ?>" placeholder="Su correo electrónico" value="<?= isset($data['email']) ? $data['email'] : ''; ?>">
                    <?php if (isset($data['email_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['email_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            ¡Email válido!
                        </div>
                    <?php endif; ?>
                </div>
                <div class="form-group">
                    <label for="password">Contraseña: <sup>*</sup></label>
                    <input type="password" name="password" class="form-control <?php if (isset($data['password_err']) && $data['password_err'] === '') {
                                                                                    echo '';
                                                                                } elseif (isset($data['password_err']) && $data['password_err'] !== '') {
                                                                                    echo 'is-invalid';
                                                                                } else {
                                                                                    echo 'is-valid';
                                                                                } ?> ?>" placeholder="Su contraseña" value="<?= isset($data['password']) ? $data['password'] : ''; ?>">
                    <?php if (isset($data['password_err'])) : ?>
                        <div id="validationServerUsernameFeedback" class="invalid-feedback">
                            <?= $data['password_err']; ?>
                        </div>
                    <?php else : ?>
                        <div class="valid-feedback">
                            !Contraseña válida!
                        </div>
                    <?php endif; ?>
                </div>
                <div class="row">
                    <div class="col">
                        <a href="<?= URLROOT . '/users/register' ?>">¿No tienes cuenta? Regístrate</a>
                    </div>
                    <div class="col">
                        <input type="submit" value="Iniciar sesión" class="btn btn-primary btn-block">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<?php require APPROOT . '/views/partials/footer.php'; ?>