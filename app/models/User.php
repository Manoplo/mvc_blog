<?php

class User
{
    // Database
    private $db;

    public function __construct()
    {
        // Instancia db
        $this->db = new Database();
    }

    public function findUserByEmail($email)
    {
        $this->db->query('SELECT * from users WHERE email = :email');
        $this->db->bind(':email', $email);
        $row = $this->db->getOneResult('User');

        if (empty($row)) {
            return false;
        } else {
            return true;
        }
    }

    public function register($data)
    {
        $this->db->query("INSERT into users(name, email, password) values(:name, :email, :password)");
        $this->db->bind(':name', $data['name']);
        $this->db->bind(':email', $data['email']);
        $this->db->bind(':password', password_hash($data['password'], PASSWORD_DEFAULT));
        $result = $this->db->execute();


        if ($result) {
            return true;
        } else {
            return false;
        }
    }

    public function login($email, $password)
    {

        $this->db->query("SELECT * from users where email = :email");
        $this->db->bind(':email', $email);

        $row = $this->db->getOneResult('User');

        if (password_verify($password, $row->password)) {
            return $row;
        } else {
            return false;
        }
    }

    public function getUserById($id){
        $this->db->query("SELECT * from users where id = :id");
        $this->db->bind(':id', $id);

        $row = $this->db->getOneResult('User');

        return $row;
    }
}
